import Dispatcher from '../dispatcher';
import ActionTypes from '../constants';

class ConfigurationActions {

	addNewDrink(drink) {
		Dispatcher.dispatch({
			actionType: ActionTypes.ADD_NEW_DRINK,
			payload: drink 
		});
	}
	setNumberOfDrinks(numberOfDrinks) {
		Dispatcher.dispatch({
			actionType: ActionTypes.SET_NUMBER_OF_DRINKS,
			payload: numberOfDrinks
		});
	}
	setRefreshingTimeInterval(refreshingTimeInterval) {
		Dispatcher.dispatch({
			actionType: ActionTypes.SET_REFRESHING_TIME_INTERVAL,
			payload: refreshingTimeInterval 
		});
	}
}

export default new ConfigurationActions();