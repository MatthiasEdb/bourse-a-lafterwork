import Dispatcher from '../dispatcher';
import ActionTypes from '../constants';

class DrinkActions {

	setNewPrices(drinkArray) {
		Dispatcher.dispatch({
			actionType: ActionTypes.SET_NEW_PRICES,
			payload: drinkArray 
		});
	}
}

export default new DrinkActions();